<%-- 
    Document   : definicion
    Created on : 08-05-2021, 16:38:21
    Author     : Paty
--%>

<%@page import="root.entity.Diccionario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Diccionario definicion = (Diccionario) request.getAttribute("definicion");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Definicion </title>
    </head>
    <body>
        <form  name="form" action="controlador" method="POST">
            <h1><%= definicion.getPalabra().toString()%></h1>
            <h2><%= definicion.getSignificado().toString()%></h2>
            <h2>Fuente: Oxford Dictionaries</h2>
            <button type="submit" name="accion" value="volver" class="btn btn-success">Volver</button>
        </form>
    </body>
</html>
