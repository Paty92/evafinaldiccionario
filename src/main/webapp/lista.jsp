<%-- 
    Document   : lista
    Created on : 08-05-2021, 0:39:11
    Author     : Paty
--%>

<%@page import="java.util.Iterator"%>
<%@page import="root.entity.Diccionario"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Diccionario> diccionario = (List<Diccionario>) request.getAttribute("listadiccionario");
    Iterator<Diccionario> itdiccionario = diccionario.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <form  name="form" action="controlador" method="POST">
            <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">


                <table border="1">
                    <thead>
                    <th>Palabra</th>
                    <th>Fecha </th>
                    <th>Significado </th>

                    </thead>
                    </tbody>
                    <%while (itdiccionario.hasNext()) {
                            Diccionario cm = itdiccionario.next();%>
                    <tr>
                        <td><%= cm.getPalabra()%></td>
                        <td><%= cm.getFecha()%></td>
                        <td><%= cm.getSignificado()%></td>
                        <td> <input type="radio" name="seleccion" value="<%= cm.getPalabra()%>"> </td>
                    </tr>
                    <%}%>   
                    </tbody>   
                </table>
  <button type="submit" name="accion" value="volver" class="btn btn-success">Volver</button>
    </body>
</html>
