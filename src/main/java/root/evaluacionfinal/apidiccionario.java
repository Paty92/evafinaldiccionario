/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacionfinal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.dao.DiccionarioJpaController;
import root.entity.Diccionario;
import root.dto.PalabraDTO;
import java.util.concurrent.ThreadLocalRandom;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

/**
 *
 * @author Paty
 */
@Path("significados")
public class apidiccionario {
     
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response significado(@PathParam("idbuscar") String idbuscar){
        
          try {
            Client client = ClientBuilder.newClient();
    
 WebTarget myResource = client.target("https://od-api.oxforddictionaries.com:443/api/v2/entries/es/" + idbuscar);
 
            PalabraDTO palabradto = myResource.request(MediaType.APPLICATION_JSON).header("app_id", "	00f2a515").header("app_key", "	307735b6913b196e0b8f82d314b2286b").get(PalabraDTO.class);
            

Diccionario diccionario = new Diccionario();

diccionario.setPalabra(palabradto.getWord());

Date fecha = new Date();

diccionario.setFecha(fecha.toString());
  Random rand = new Random();
            int upperbound = 1000;
              int intRandom = rand.nextInt(upperbound); 
        
            String s = String.valueOf(intRandom);
            diccionario.setPalabra(s);
            
    String Significado = (String) palabradto.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
           diccionario.setSignificado(Significado);
           
 DiccionarioJpaController dao = new  DiccionarioJpaController();
 dao.create(diccionario);
 
  return Response.ok(200).entity(diccionario).build();
    } catch (Exception ex) {
            Logger.getLogger(apidiccionario.class.getName()).log(Level.SEVERE, null, ex);
        
       }
        return null;
    }
     @GET
    @Path("/historial")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listar(){
         DiccionarioJpaController dao = new  DiccionarioJpaController();
         
             List<Diccionario> lista = dao.findDiccionarioEntities();
               return Response.ok(200).entity(lista).build();
    }
    }
 