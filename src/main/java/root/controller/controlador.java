/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import root.dao.DiccionarioJpaController;
import root.entity.Diccionario;

/**
 *
 * @author Paty
 */
@WebServlet(name = "controlador", urlPatterns = {"/controlador"})
public class controlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet controlador</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet controlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         System.out.println("doPost");
        String accion = request.getParameter("accion");
          String idbuscar = request.getParameter("idbuscar");
          
          
        
        if (accion.equals("ingreso")) {
             Client client = ClientBuilder.newClient();
              WebTarget myResource = client.target("http://192.168.1.95:8080/evaluacionfinal-1.0-SNAPSHOT/api/significados/" + idbuscar);

              Diccionario diccionario = (Diccionario) myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<Diccionario>() {
                  
               });

            try {

                String palabra = request.getParameter("palabra");
                String significado = request.getParameter("significado");
                String fecha = request.getParameter("fecha");

                Diccionario diccionario1 = new Diccionario();

                diccionario1.setPalabra(palabra);
                diccionario1.setFecha(fecha);
                diccionario1.setSignificado(significado);
                
        
                DiccionarioJpaController dao = new DiccionarioJpaController();

                dao.create(diccionario1);

          
                    } catch (Exception ex) {
            Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
                   request.setAttribute("diccionario", diccionario);
               request.getRequestDispatcher("definicion.jsp").forward(request, response);
        }
        
            
        if (accion.equals("lista")) {
            

            Client client = ClientBuilder.newClient();
              WebTarget myResource = client.target("http://192.168.1.95:8080/evaluacionfinal-1.0-SNAPSHOT/api/significados/historial");


              List<Diccionario> lista = (List<Diccionario>) myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Diccionario>>() {
            });
           

            request.setAttribute("listadiccionario", lista);
            request.getRequestDispatcher("lista.jsp").forward(request, response);

        }
        
          if (accion.equals("volver")) {
              
              try {
                  
                  String palabra = request.getParameter("palabra");
                  String fecha = request.getParameter("fecha");
                  String significado = request.getParameter("significado");
                  
                  Diccionario diccionario = new Diccionario();
                  diccionario.setPalabra(palabra);
                  diccionario.setFecha(fecha);
                  diccionario.setSignificado(significado);
              
                   DiccionarioJpaController dao = new DiccionarioJpaController();
                   dao.create(diccionario);
                    } catch (Exception ex) {
                        
                Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
             request.getRequestDispatcher("index.jsp").forward(request, response);
            
              
              
            } 
        processRequest(request, response);
    }
    
    /**
     * }
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
